package com.navdy.hardware;

import java.io.IOException;
import java.nio.ByteBuffer;

public class SerialPort {
    private static final String TAG = "SerialPort";
    private int mNativeContext;
    private final String mPath;

    private static native void classInitNative();

    private native void native_close();

    private native boolean native_open(String str, int i) throws IOException;

    private native int native_read_array(byte[] bArr, int i) throws IOException;

    private native int native_read_direct(ByteBuffer byteBuffer, int i) throws IOException;

    private native void native_send_break();

    private native void native_write_array(byte[] bArr, int i) throws IOException;

    private native void native_write_direct(ByteBuffer byteBuffer, int i) throws IOException;

    static {
        System.loadLibrary("obd-service");
        classInitNative();
    }

    public SerialPort(String name) {
        this.mPath = name;
    }

    public boolean open(int speed) throws IOException {
        return native_open(this.mPath, speed);
    }

    public void close() throws IOException {
        native_close();
    }

    public String getPath() {
        return this.mPath;
    }

    public int read(ByteBuffer buffer) throws IOException {
        if (buffer.isDirect()) {
            return native_read_direct(buffer, buffer.remaining());
        }
        if (buffer.hasArray()) {
            return native_read_array(buffer.array(), buffer.remaining());
        }
        throw new IllegalArgumentException("buffer is not direct and has no array");
    }

    public void write(ByteBuffer buffer, int length) throws IOException {
        if (buffer.isDirect()) {
            native_write_direct(buffer, length);
        } else if (buffer.hasArray()) {
            native_write_array(buffer.array(), length);
        } else {
            throw new IllegalArgumentException("buffer is not direct and has no array");
        }
    }

    public void sendBreak() {
        native_send_break();
    }
}
