package com.navdy.os;

public class SystemProperties {
    private static Class<?> CLASS;

    static {
        try {
            CLASS = Class.forName("android.os.SystemProperties");
        } catch (ClassNotFoundException e) {
        }
    }

    public static String get(String key) {
        try {
            return (String) CLASS.getMethod("get", new Class[]{String.class}).invoke(null, new Object[]{key});
        } catch (Exception e) {
            return null;
        }
    }

    public static String get(String key, String def) {
        try {
            return (String) CLASS.getMethod("get", new Class[]{String.class, String.class}).invoke(null, new Object[]{key, def});
        } catch (Exception e) {
            return def;
        }
    }

    public static int getInt(String key, int def) {
        try {
            def = ((Integer) CLASS.getMethod("getInt", new Class[]{String.class, Integer.TYPE}).invoke(null, new Object[]{key, Integer.valueOf(def)})).intValue();
        } catch (Exception e) {
        }
        return def;
    }

    public static long getLong(String key, long def) {
        try {
            def = ((Long) CLASS.getMethod("getLong", new Class[]{String.class, Long.TYPE}).invoke(null, new Object[]{key, Long.valueOf(def)})).longValue();
        } catch (Exception e) {
        }
        return def;
    }

    public static boolean getBoolean(String key, boolean def) {
        try {
            def = ((Boolean) CLASS.getMethod("getBoolean", new Class[]{String.class, Boolean.TYPE}).invoke(null, new Object[]{key, Boolean.valueOf(def)})).booleanValue();
        } catch (Exception e) {
        }
        return def;
    }

    private SystemProperties() {
    }
}
