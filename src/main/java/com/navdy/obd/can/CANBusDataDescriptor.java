package com.navdy.obd.can;

public class CANBusDataDescriptor {
    public int bitOffset;
    public int byteOffset;
    public int dataFrequency;
    public String header;
    public int lengthInBits;
    public boolean littleEndian;
    public double maxValue;
    public double minValue;
    public String name;
    public int obdPid;
    public double resolution;
    public long valueOffset;

    public CANBusDataDescriptor(String name, String header, int byteOffset, int bitOffset, int lengthInBits, double resolution, long valueOffset, double minValue, double maxValue, int obd2Pid, int dataFrequency, boolean littleEndian) {
        this.name = name;
        this.header = header;
        this.byteOffset = byteOffset;
        this.bitOffset = bitOffset;
        this.lengthInBits = lengthInBits;
        this.resolution = resolution;
        this.valueOffset = valueOffset;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.obdPid = obd2Pid;
        this.dataFrequency = dataFrequency;
        this.littleEndian = littleEndian;
    }
}
