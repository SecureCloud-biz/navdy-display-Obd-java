package com.navdy.obd;

import android.util.Log;

public class InstantFuelConsumptionPidProcessor extends PidProcessor {
    public static final int FUEL_STATUS_OPEN_LOOP = 4;
    private static final double MINIMUM_FUEL_CONSUMPTION_WHEN_DECELERATING = 2.35214d;
    public static final String TAG = VehicleStateManager.class.getSimpleName();
    private boolean mHasFuelSystemStatus;
    private boolean mHasThrottlePosition;
    private double mMinThrottlePosition = 100.0d;

    public boolean isSupported(PidSet supportedPids) {
        this.mHasFuelSystemStatus = supportedPids.contains(3);
        this.mHasThrottlePosition = supportedPids.contains(17);
        return supportedPids.contains(16) && supportedPids.contains(13);
    }

    public boolean processPidValue(PidLookupTable vehicleState) {
        double cons;
        double maf = vehicleState.getPidValue(16);
        double vehicleSpeed = vehicleState.getPidValue(13);
        if (this.mHasFuelSystemStatus && this.mHasThrottlePosition) {
            double throttlePosition = vehicleState.getPidValue(17);
            short fuelSystemStatus = (short) ((int) vehicleState.getPidValue(3));
            if (throttlePosition < this.mMinThrottlePosition && throttlePosition > 0.0d) {
                this.mMinThrottlePosition = throttlePosition;
            }
            if (throttlePosition >= this.mMinThrottlePosition || fuelSystemStatus != (short) 4) {
                cons = Utility.calculateInstantaneousFuelConsumption(maf, vehicleSpeed);
            } else {
                Log.d(TAG, "Vehicle is decelerating");
                cons = MINIMUM_FUEL_CONSUMPTION_WHEN_DECELERATING;
            }
        } else {
            cons = Utility.calculateInstantaneousFuelConsumption(maf, vehicleSpeed);
        }
        return vehicleState.updatePid(256, cons);
    }

    public PidSet resolveDependencies() {
        PidSet requiredPids = new PidSet();
        requiredPids.add(16);
        requiredPids.add(13);
        if (this.mHasFuelSystemStatus) {
            requiredPids.add(3);
        }
        if (this.mHasThrottlePosition) {
            requiredPids.add(17);
        }
        return requiredPids;
    }
}
