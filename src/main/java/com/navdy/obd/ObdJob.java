package com.navdy.obd;

import com.navdy.obd.command.ICommand;
import com.navdy.obd.command.IObdDataObserver;
import com.navdy.obd.command.InitializeCommand;
import com.navdy.obd.io.IChannel;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObdJob implements Runnable {
    static final Logger Log = LoggerFactory.getLogger(ObdJob.class);
    protected IChannel channel;
    protected ICommand command;
    protected IObdDataObserver commandObserver;
    protected boolean disconnectUponFailure;
    protected IListener listener;
    protected Protocol protocol;

    public interface IListener {
        void onComplete(ObdJob obdJob, boolean z);
    }

    public ObdJob(ICommand command, IChannel channel, Protocol protocol, IListener listener, IObdDataObserver commandObserver, boolean disconnectUponFailure) {
        this.disconnectUponFailure = false;
        this.command = command;
        this.channel = channel;
        this.listener = listener;
        this.protocol = protocol;
        this.commandObserver = commandObserver;
        this.disconnectUponFailure = disconnectUponFailure;
    }

    public ObdJob(ICommand command, IChannel channel, Protocol protocol, IObdDataObserver commandObserver) {
        this(command, channel, protocol, null, commandObserver, true);
    }

    public ObdJob(ICommand command, IChannel channel, Protocol protocol, IListener listener, IObdDataObserver commandObserver) {
        this(command, channel, protocol, listener, commandObserver, true);
    }

    protected void postExecute() throws IOException {
    }

    public void run() {
        Log.debug("executing command {}", this.command.getName());
        boolean succeeded = false;
        try {
            this.command.execute(this.channel.getInputStream(), this.channel.getOutputStream(), this.protocol, this.commandObserver);
            Log.debug("response {}", this.command.getResponse());
            postExecute();
            succeeded = true;
        } catch (Exception e) {
            if (this.command instanceof InitializeCommand) {
                Log.error("InitializeCommand failed, with an IOException. " + e.getMessage());
            } else {
                Log.error("I/O error {} executing command {} - closing channel", e.toString(), this.command.getName(), e);
                Log.error("Response received {}", this.command.getResponse());
            }
            if (this.disconnectUponFailure) {
                this.channel.disconnect();
            }
        }
        if (this.listener != null) {
            this.listener.onComplete(this, succeeded);
        }
    }
}
