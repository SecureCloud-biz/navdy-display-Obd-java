package com.navdy.obd.command;

public class ReadVoltageCommand extends ObdCommand {
    public ReadVoltageCommand() {
        super("atrv");
    }

    public float getVoltage() {
        String response = getResponse().trim();
        if (response.endsWith("V")) {
            response = response.substring(0, response.length() - 1);
        }
        Float voltage = 0.0f;
        try {
            voltage = Float.parseFloat(response);
        } catch (NumberFormatException e) {
            Log.warn("Unable to parse ({}) as float voltage", response);
        }
        return voltage;
    }
}
