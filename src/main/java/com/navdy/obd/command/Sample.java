package com.navdy.obd.command;

import com.navdy.obd.Pid;
import com.navdy.util.RunningStats;
import kotlin.Metadata;
import kotlin.jvm.JvmField;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u00020\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0010\u0010\u0002\u001a\u00020\r8\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\u000f8\u0006X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0012\u0010\u0012\u001a\u00020\u00138\u0006@\u0006X\u000e¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lcom/navdy/obd/command/Sample;", "", "pid", "", "scanInterval", "(II)V", "lastSampleTimestamp", "", "lastSamplingTime", "getLastSamplingTime", "()J", "setLastSamplingTime", "(J)V", "Lcom/navdy/obd/Pid;", "samplingTimeStats", "Lcom/navdy/util/RunningStats;", "getScanInterval", "()I", "updated", "", "obd-service_release"}, k = 1, mv = {1, 1, 6})
/* compiled from: Sample.kt */
public final class Sample {
    @JvmField
    public long lastSampleTimestamp;
    private long lastSamplingTime;
    @NotNull
    @JvmField
    public final Pid pid;
    @NotNull
    @JvmField
    public final RunningStats samplingTimeStats = new RunningStats();
    private final int scanInterval;
    @JvmField
    public boolean updated;

    public Sample(int pid, int scanInterval) {
        this.scanInterval = scanInterval;
        this.pid = new Pid(pid);
    }

    public final int getScanInterval() {
        return this.scanInterval;
    }

    public final long getLastSamplingTime() {
        return this.lastSamplingTime;
    }

    public final void setLastSamplingTime(long samp) {
        this.lastSamplingTime = samp;
    }
}
