package com.navdy.obd.command;

public interface IObdDataObserver {
    void onCommand(String str);

    void onError(String str);

    void onRawCanBusMessage(String str);

    void onResponse(String str);
}
