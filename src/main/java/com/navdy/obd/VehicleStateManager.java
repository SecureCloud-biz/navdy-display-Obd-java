package com.navdy.obd;

import android.util.SparseArray;
import com.navdy.obd.ScanSchedule.Scan;
import java.util.List;
import java.util.Map.Entry;

public class VehicleStateManager {
    public static final String TAG = VehicleStateManager.class.getSimpleName();
    private PidLookupTable mObdSnapshot = new PidLookupTable(320);
    private PidProcessorFactory mPidProcessorFactory;
    private SparseArray<PidProcessor> mPidProcessors = new SparseArray(64);

    public void updateSupportedPids(PidSet supportedPids) {
        if (this.mPidProcessorFactory != null) {
            for (Integer pid : this.mPidProcessorFactory.getPidsHavingProcessors()) {
                PidProcessor processor = this.mPidProcessorFactory.buildPidProcessorForPid(pid.intValue());
                if (processor != null && processor.isSupported(supportedPids)) {
                    this.mPidProcessors.append(pid.intValue(), processor);
                    if (!supportedPids.contains(pid.intValue())) {
                        supportedPids.add(pid.intValue());
                    }
                }
            }
        }
    }

    public VehicleStateManager(PidProcessorFactory factory) {
        this.mPidProcessorFactory = factory;
    }

    public PidLookupTable getObdSnapshot() {
        return this.mObdSnapshot;
    }

    public void update(List<Pid> pidList) {
        this.mObdSnapshot.build(pidList);
    }

    public void onScanComplete() {
        for (int i = 0; i < this.mPidProcessors.size(); i++) {
            ((PidProcessor) this.mPidProcessors.valueAt(i)).processPidValue(this.mObdSnapshot);
        }
    }

    public void resolveDependenciesForCustomPids(ScanSchedule schedule) {
        if (schedule != null) {
            ScanSchedule customPids = new ScanSchedule();
            for (Entry<Integer, Scan> entry : schedule.schedule.entrySet()) {
                Scan scan = (Scan) entry.getValue();
                if (scan.pid >= 256) {
                    PidProcessor processor = (PidProcessor) this.mPidProcessors.get(scan.pid);
                    if (processor != null) {
                        PidSet dependencies = processor.resolveDependencies();
                        if (dependencies != null) {
                            customPids.addPids(dependencies.asList(), scan.scanInterval);
                        }
                    }
                }
            }
            if (customPids.size() > 0) {
                schedule.merge(customPids);
            }
        }
    }
}
