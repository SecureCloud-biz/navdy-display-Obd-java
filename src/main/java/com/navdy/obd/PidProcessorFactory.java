package com.navdy.obd;

import java.util.List;

public interface PidProcessorFactory {
    PidProcessor buildPidProcessorForPid(int i);

    List<Integer> getPidsHavingProcessors();
}
