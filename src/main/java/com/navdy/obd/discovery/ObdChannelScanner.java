package com.navdy.obd.discovery;

import android.content.Context;
import com.navdy.obd.io.ChannelInfo;

public abstract class ObdChannelScanner {
    protected Context mContext;
    protected Listener mListener;

    public interface Listener {
        void onDiscovered(ObdChannelScanner obdChannelScanner, ChannelInfo channelInfo);

        void onScanStarted(ObdChannelScanner obdChannelScanner);

        void onScanStopped(ObdChannelScanner obdChannelScanner);
    }

    public abstract boolean startScan();

    public abstract boolean stopScan();

    ObdChannelScanner(Context context, Listener listener) {
        this.mContext = context;
        this.mListener = listener;
    }
}
