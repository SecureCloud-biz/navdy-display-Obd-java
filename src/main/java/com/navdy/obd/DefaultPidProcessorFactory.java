package com.navdy.obd;

import java.util.ArrayList;
import java.util.List;

public class DefaultPidProcessorFactory implements PidProcessorFactory {
    private static List<Integer> PIDS_WITH_PRE_PROCESSORS = new ArrayList();

    static {
        PIDS_WITH_PRE_PROCESSORS.add(Integer.valueOf(Pids.INSTANTANEOUS_FUEL_CONSUMPTION));
        PIDS_WITH_PRE_PROCESSORS.add(Integer.valueOf(Pids.FUEL_LEVEL));
        PIDS_WITH_PRE_PROCESSORS.add(Integer.valueOf(Pids.VEHICLE_SPEED));
    }

    public PidProcessor buildPidProcessorForPid(int pid) {
        switch (pid) {
            case Pids.VEHICLE_SPEED:
                return new SpeedPidProcessor();
            case 47:
            case Pids.FUEL_LEVEL:
                return new FuelLevelPidProcessor();
            case Pids.INSTANTANEOUS_FUEL_CONSUMPTION:
                return new InstantFuelConsumptionPidProcessor();
            default:
                return null;
        }
    }

    public List<Integer> getPidsHavingProcessors() {
        return PIDS_WITH_PRE_PROCESSORS;
    }
}
