package com.navdy.obd.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface IChannel {
    public static final int READ_TIMED_OUT = -2;
    public static final int STATE_CONNECTED = 2;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_IDLE = 4;
    public static final int STATE_INITIALIZING = 3;
    public static final int STATE_SLEEPING = 5;

    void connect(String str);

    void disconnect();

    InputStream getInputStream() throws IOException;

    OutputStream getOutputStream() throws IOException;

    int getState();
}
