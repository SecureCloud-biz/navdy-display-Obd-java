package com.navdy.obd.io;

import android.content.Context;
import android.text.TextUtils;

import com.navdy.hardware.SerialPort;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter;

public class STNSerialChannel implements IChannel, IChannelSink {
    private static final int CARRIAGE_RETURN = 13;
    public static final int CONNECT_TIMEOUT_MILLIS = 60000;
    private static final int DEFAULT_BAUD_RATE = 9600;
    static final Logger Log = LoggerFactory.getLogger(STNSerialChannel.class);
    public static final long NANOS_PER_MS = 1000000;
    public static final String OK = "ok";
    private static final int[] POSSIBLE_BAUD_RATES_TABLE = new int[]{9600, 19200, 38400, 57600, 115200, 230400, 460800};
    public static final String SET_BAUD_RATE_COMMAND = "STSBR";
    public static final int TIMEOUT_MILLIS = 3000;
    private static ExecutorService singleThreadedExecutor = Executors.newSingleThreadExecutor();
    private int baudRate = 9600;
    private final Context context;
    private Future future;
    private int lastConnectedBaudRate;
    private SerialChannel serialChannel;
    private final IChannelSink sink;
    private volatile int state = 0;

    public STNSerialChannel(Context context, IChannelSink sink, int lastConnectedBaudRate, int baudRate) {
        this.context = context;
        this.sink = sink;
        this.baudRate = baudRate;
        this.lastConnectedBaudRate = lastConnectedBaudRate;
    }

    public int getState() {
        return this.state;
    }

    private void setState(int newState) {
        if (newState != this.state) {
            this.state = newState;
            if (this.sink != null) {
                this.sink.onStateChange(newState);
            }
        }
    }

    public void connect(final String address) {
        Log.debug("connect " + address + ", Last connected Baud :" + this.lastConnectedBaudRate + ", Required :" + this.baudRate);
        if (this.state == 1) {
            setState(1);
            return;
        }
        this.future = singleThreadedExecutor.submit(new Runnable() {
            public void run() {
                STNSerialChannel.this.setState(1);
                ArrayList<Integer> possibleBaudRatesList = new ArrayList();
                possibleBaudRatesList.add(Integer.valueOf(STNSerialChannel.this.lastConnectedBaudRate));
                if (STNSerialChannel.this.baudRate != STNSerialChannel.this.lastConnectedBaudRate) {
                    possibleBaudRatesList.add(Integer.valueOf(STNSerialChannel.this.baudRate));
                }
                for (int possibleBaudRate : STNSerialChannel.POSSIBLE_BAUD_RATES_TABLE) {
                    if (!(possibleBaudRate == STNSerialChannel.this.lastConnectedBaudRate || possibleBaudRate == STNSerialChannel.this.baudRate)) {
                        possibleBaudRatesList.add(possibleBaudRate);
                    }
                }
                int successBaudrate = -1;
                for (int possibleBaudRate : possibleBaudRatesList) {
                    STNSerialChannel.Log.debug("Trying to connect at baud rate : " + possibleBaudRate);
                    boolean success = STNSerialChannel.this.tryConnectingAtBaudRate(address, possibleBaudRate);
                    if (!Thread.interrupted()) {
                        if (success) {
                            successBaudrate = possibleBaudRate;
                            break;
                        }
                    }
                    STNSerialChannel.Log.debug("Connection attempt has been interrupted, Attempt , Baud rate : " + possibleBaudRate + ", Success : " + success);
                    STNSerialChannel.this.setState(0);
                    return;
                }
                if (successBaudrate == -1) {
                    STNSerialChannel.Log.debug("Not able to communicate with chip at any baud rate");
                    STNSerialChannel.this.setState(0);
                } else if (successBaudrate == STNSerialChannel.this.baudRate) {
                    STNSerialChannel.this.onSuccess(address);
                } else {
                    STNSerialChannel.Log.debug("Successfully connected at Baud rate " + successBaudrate + " , Trying to change to required baud rate");
                    if (STNSerialChannel.this.setBaudRateAndConnect(address, successBaudrate, STNSerialChannel.this.baudRate)) {
                        STNSerialChannel.this.onSuccess(address);
                    } else {
                        STNSerialChannel.this.setState(0);
                    }
                }
            }
        });
        try {
            this.future.get(ReconfigureOnChangeFilter.DEFAULT_REFRESH_PERIOD, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Log.error("Execution InterruptedException", e);
            Log.debug("Cancel task " + this.future.cancel(true));
            setState(0);
        } catch (ExecutionException e2) {
            Log.error("ExecutionException during execution", e2);
            setState(0);
        } catch (TimeoutException e22) {
            Log.error("TimeoutException during execution", e22);
            setState(0);
        }
    }

    private void onSuccess(String address) {
        Log.debug("Success");
        this.serialChannel = new SerialChannel(this.context, this, this.baudRate);
        this.serialChannel.connect(address);
    }

    private boolean setBaudRateAndConnect(String address, int currentBaudRate, int requiredBaud) {
        boolean z = false;
        SerialChannel serialChannel = new SerialChannel(this.context, this, currentBaudRate);
        serialChannel.connect(address);
        StringBuilder commandBuilder = new StringBuilder();
        commandBuilder.append(SET_BAUD_RATE_COMMAND).append(" ").append(requiredBaud);
        try {
            runCommand("ate0", serialChannel, 3000);
            String response = runCommand(commandBuilder.toString(), serialChannel, 3000);
            if (OK.equals(response)) {
                serialChannel.disconnect();
                serialChannel = new SerialChannel(this.context, this, requiredBaud);
                serialChannel.connect(address);
                try {
                    runCommand("\n", serialChannel, 3000);
                    response = runCommand("stwbr", serialChannel, 3000);
                    if (TextUtils.isEmpty(response) || !response.trim().toLowerCase().equals(OK)) {
                        Log.debug("Unexpected response for STWBR command " + (TextUtils.isEmpty(response) ? "null" : response.trim()));
                        serialChannel.disconnect();
                        return false;
                    }
                    z = true;
                    return z;
                } catch (Exception e) {
                    Log.error("Exception while runCommand " + e.getMessage());
                    return z;
                } finally {
                    serialChannel.disconnect();
                }
            } else {
                Log.debug("Setting baud rate , failed with response " + response);
                return false;
            }
        } catch (Exception e2) {
            Log.error("Exception while runCommand " + e2.getMessage());
        } finally {
            serialChannel.disconnect();
        }
        return z;
    }

    private boolean tryConnectingAtBaudRate(String address, int baudRate) {
        SerialChannel serialChannel = new SerialChannel(this.context, this, baudRate);
        serialChannel.connect(address);
        try {
            String response = runCommand("ati", serialChannel, 3000);
            return true;
        } catch (Exception e) {
            Log.error("IOException while runCommand " + e.getMessage());
            return false;
        } finally {
            serialChannel.disconnect();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private String runCommand(String command, SerialChannel serialChannel, long timeoutMillis) throws IOException, TimeoutException, InterruptedException {
        if (command == null || command.length() == 0) {
            Log.debug("Empty command " + command);
            return null;
        }
        int length = command.length();
        byte[] commandBytes = new byte[(length + 1)];
        System.arraycopy(command.getBytes(), 0, commandBytes, 0, length);
        commandBytes[commandBytes.length - 1] = (byte) 13;
        InputStream inputStream = serialChannel.getInputStream();
        OutputStream outputStream = serialChannel.getOutputStream();
        outputStream.write(commandBytes);
        outputStream.flush();
        int b = -1;
        StringBuilder responseBuilder = new StringBuilder();
        boolean textResponse = false;
        long start = System.nanoTime();
        long elapsedTime = 0;
        long timeoutNanos = timeoutMillis * 1000000;
        boolean setBaudRateCommand = command.startsWith(SET_BAUD_RATE_COMMAND);
        while (!Thread.interrupted()) {
            b = inputStream.read();
            if (b != -1 && b != 62) {
                elapsedTime = System.nanoTime() - start;
                if (elapsedTime >= timeoutNanos) {
                    break;
                } else if (b != -2) {
                    boolean validHexDigit = (b >= 48 && b <= 57) || ((b >= 65 && b <= 70) || (b >= 97 && b <= 102));
                    boolean isWhiteSpace = b == 32 || b == 13;
                    if (!(validHexDigit || isWhiteSpace)) {
                        textResponse = true;
                    }
                    if (textResponse || b != 32) {
                        responseBuilder.append((char) b);
                    }
                    if (setBaudRateCommand) {
                        String response = responseBuilder.toString().trim().toLowerCase();
                        if (OK.equals(response)) {
                            return OK;
                        }
                        Log.error("Unexpected response for the set baud rate command : " + response);
                    }
                }
            } else {
                break;
            }
        }
        if (elapsedTime > timeoutNanos) {
            throw new IOException("Timeout after " + (elapsedTime / 1000000) + " ms");
        }
        Log.debug("Flushing out remaining data");
        int lastChar = b;
        while (true) {
            b = inputStream.read();
            if (b == -1 || Thread.interrupted()) {
                break;
            } else if (b == -2) {
                break;
            } else if (b >= 0) {
                lastChar = b;
                responseBuilder.append((char) b);
            }
        }
        if (Thread.interrupted()) {
            Log.debug("Thread was interrupted ");
            throw new InterruptedException();
        } else if (lastChar != 62) {
            throw new IOException("Unexpected end of stream");
        } else {
            Log.debug("Response for command : " + command + " is : " + responseBuilder.toString().trim());
            return responseBuilder.toString();
        }
    }


    public void disconnect() {
        if (this.serialChannel != null) {
            this.serialChannel.disconnect();
            return;
        }
        Log.debug("disconnect when trying to connect, aborting");
        this.future.cancel(true);
    }

    public InputStream getInputStream() throws IOException {
        if (this.serialChannel != null) {
            return this.serialChannel.getInputStream();
        }
        return null;
    }

    public OutputStream getOutputStream() throws IOException {
        if (this.serialChannel != null) {
            return this.serialChannel.getOutputStream();
        }
        return null;
    }

    public SerialPort getSerialPort() {
        if (this.serialChannel != null) {
            return this.serialChannel.getSerialPort();
        }
        return null;
    }

    public void onStateChange(int newState) {
        if (this.serialChannel != null) {
            setState(newState);
        }
    }

    public int getBaudRate() {
        return this.baudRate;
    }

    public void onMessage(String message) {
        if (this.serialChannel != null) {
            Log.debug("Forwarding message to main sink , Message : " + message);
            if (this.sink != null) {
                this.sink.onMessage(message);
                return;
            }
            return;
        }
        Log.debug("Internal message , message : " + this.state);
    }
}
