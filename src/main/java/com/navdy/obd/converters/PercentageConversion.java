package com.navdy.obd.converters;

public class PercentageConversion extends LinearCombinationConversion {
    public PercentageConversion() {
        super(100, 0, 0, 255.0d);
    }
}
