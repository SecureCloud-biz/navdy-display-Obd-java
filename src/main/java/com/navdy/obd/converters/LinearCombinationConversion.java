package com.navdy.obd.converters;

public class LinearCombinationConversion implements AbstractConversion {
    private int aBias;
    private int aFactor;
    private int bBias;
    private int bFactor;
    private double denominator;

    public LinearCombinationConversion(int aFactor, int aBias, int bFactor, double denominator) {
        this.aFactor = aFactor;
        this.aBias = aBias;
        this.bFactor = bFactor;
        this.denominator = denominator;
    }

    public double convert(byte[] rawData) {
        if (rawData == null || rawData.length < 3) {
            return 0.0d;
        }
        int A = rawData[2] & 255;
        int B = 0;
        if (this.bFactor != 0 && rawData.length > 3) {
            B = rawData[3] & 255;
        }
        return ((double) ((this.aFactor * (this.aBias + A)) + (this.bFactor * B))) / this.denominator;
    }
}
