package com.navdy.obd;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.navdy.obd.ScanSchedule.Scan;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Map.Entry;

public class PidSet implements Parcelable {
    public static final Creator<PidSet> CREATOR = new Creator<PidSet>() {
        public PidSet createFromParcel(Parcel source) {
            return new PidSet(source);
        }

        public PidSet[] newArray(int size) {
            return new PidSet[size];
        }
    };
    public static final int MAX_PID = 320;
    private BitSet pids;

    public PidSet() {
        this.pids = new BitSet(320);
    }

    public PidSet(long bitField, int offset) {
        this();
        for (int bit = 0; bit < 32; bit++) {
            if ((bitField & ((long) (1 << (31 - bit)))) != 0) {
                this.pids.set((offset + bit) + 1);
            }
        }
    }

    public PidSet(ScanSchedule schedule) {
        this();
        for (Entry<Integer, Scan> entry : schedule.schedule.entrySet()) {
            this.pids.set(((Scan) entry.getValue()).pid);
        }
    }

    public PidSet(List<Pid> pids) {
        this();
        for (Pid pid : pids) {
            this.pids.set(pid.getId());
        }
    }

    public void add(Pid pid) {
        add(pid.getId());
    }

    public void remove(int id) {
        this.pids.clear(id);
    }

    public void add(int id) {
        this.pids.set(id);
    }

    public boolean contains(Pid pid) {
        return this.pids.get(pid.getId());
    }

    public boolean contains(int pidId) {
        return this.pids.get(pidId);
    }

    public void merge(PidSet pidSet) {
        this.pids.or(pidSet.pids);
    }

    public void clear() {
        this.pids.clear();
    }

    public boolean isEmpty() {
        return this.pids.isEmpty();
    }

    public int size() {
        return this.pids.cardinality();
    }

    public List<Pid> asList() {
        List<Pid> list = new ArrayList(this.pids.cardinality());
        int index = 0;
        while (index < 320 && index != -1) {
            index = this.pids.nextSetBit(index);
            if (index != -1) {
                list.add(new Pid(index));
                index++;
            }
        }
        return list;
    }

    public int nextPid(int id) {
        return this.pids.nextSetBit(id);
    }

    public int describeContents() {
        return 0;
    }

    public PidSet(Parcel in) {
        long[] bits = new long[in.readByte()];
        in.readLongArray(bits);
        this.pids = BitSet.valueOf(bits);
    }

    public void writeToParcel(Parcel dest, int flags) {
        long[] bits = this.pids.toLongArray();
        dest.writeByte((byte) bits.length);
        dest.writeLongArray(bits);
    }

    public boolean equals(Object o) {
        return (o instanceof PidSet) && this.pids.equals(((PidSet) o).pids);
    }
}
