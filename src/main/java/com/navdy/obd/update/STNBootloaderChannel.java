package com.navdy.obd.update;

import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import com.navdy.hardware.SerialPort;
import com.navdy.obd.Utility;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeoutException;

public class STNBootloaderChannel {
    public static final int ACK = 64;
    public static final byte[] CONNECT_BOOTLOADER_COMMAND = new byte[]{(byte) 3, (byte) 0, (byte) 0};
    public static final int DEFAULT_CHUNK_SIZE = 5120;
    public static final byte DLE = (byte) 5;
    public static final byte ERROR_CODE_CRC_FAILED = (byte) -2;
    public static final byte ERROR_CODE_INVALID_RESPONSE = (byte) -1;
    public static final byte ERROR_CODE_IO_TIMED_OUT = (byte) -3;
    public static final byte ERROR_CODE_VALIDATION_ERROR = (byte) -112;
    public static final byte ETX = (byte) 4;
    public static final byte[] GET_BOOTLOADER_VERSION_COMMAND = new byte[]{(byte) 6, (byte) 0, (byte) 0};
    public static final byte[] GET_FIRMWARE_STATUS_BOOTLOADER_COMMAND = new byte[]{(byte) 15, (byte) 0, (byte) 0};
    public static final int INDEX_COMMAND = 0;
    public static final int INDEX_DATA = 2;
    public static final int INDEX_DATA_LENGTH = 1;
    public static final int MAX_RETRIES = 10;
    public static final int MINIMUM_CHUNK_SIZE = 16;
    public static final byte[] RESET_BOOTLOADER_COMMAND = new byte[]{(byte) 2, (byte) 0, (byte) 0};
    public static final byte SEND_CHUNK_COMMAND = (byte) 49;
    public static final byte[] START_UPLOAD_COMMAND = new byte[]{(byte) 48, (byte) 0, (byte) 4, (byte) 0, (byte) 0, (byte) 0, (byte) 1};
    public static final byte STX = (byte) 85;
    private static final String TAG = STNBootloaderChannel.class.getSimpleName();
    private byte[] mChunkBuffer;
    private byte[] mPacketBuffer;
    private SerialPort mSerialPort;

    public STNBootloaderChannel(SerialPort port) {
        this.mSerialPort = port;
    }

    public boolean enterBootloaderMode() throws IOException {
        clearBuffers();
        try {
            write(CONNECT_BOOTLOADER_COMMAND);
            byte[] data = read();
            Log.e(TAG, "Entered boot mode");
            return true;
        } catch (BootLoaderMessageError er) {
            Log.e(TAG, er.toString());
            return false;
        }
    }

    public byte[] getVersion() throws BootLoaderMessageError, Exception {
        write(GET_BOOTLOADER_VERSION_COMMAND);
        return read();
    }

    public boolean getFirmwareStatus() throws BootLoaderMessageError, Exception {
        write(GET_FIRMWARE_STATUS_BOOTLOADER_COMMAND);
        byte[] data = read();
        return (data == null || data.length <= 0 || data[2] == (byte) 0) ? false : true;
    }

    public int startUpload(int imageSize) throws IOException, BootLoaderMessageError {
        byte[] command = new byte[START_UPLOAD_COMMAND.length];
        System.arraycopy(START_UPLOAD_COMMAND, 0, command, 0, START_UPLOAD_COMMAND.length);
        imageSize &= ViewCompat.MEASURED_SIZE_MASK;
        command[3] = (byte) ((imageSize >> 16) & 255);
        command[4] = (byte) ((imageSize >> 8) & 255);
        command[5] = (byte) (imageSize & 255);
        write(command);
        return parseInt(read(), 2);
    }

    public int sendChunk(int chunkNum, byte[] chunk) throws BootLoaderMessageError, Exception {
        int chunkLen = chunk.length;
        int dataLength = chunkLen + 5;
        if (this.mChunkBuffer == null || this.mChunkBuffer.length != dataLength) {
            this.mChunkBuffer = new byte[dataLength];
        }
        ByteBuffer buffer = ByteBuffer.wrap(this.mChunkBuffer);
        buffer.put(SEND_CHUNK_COMMAND);
        buffer.putShort((short) ((chunkLen + 2) & SupportMenu.USER_MASK));
        buffer.putShort((short) (chunkNum & SupportMenu.USER_MASK));
        buffer.put(chunk);
        buffer.flip();
        write(this.mChunkBuffer);
        return parseInt(read(), 2);
    }

    public boolean reset() throws Exception, BootLoaderMessageError {
        clearBuffers();
        write(RESET_BOOTLOADER_COMMAND);
        return true;
    }

    public void write(byte[] data) throws IOException {
        if (data != null && data.length > 0) {
            int packetLength = (data.length * 2) + 5;
            if (this.mPacketBuffer == null || this.mPacketBuffer.length != packetLength) {
                this.mPacketBuffer = new byte[packetLength];
            }
            ByteBuffer byteBuffer = ByteBuffer.wrap(this.mPacketBuffer);
            byteBuffer.put(STX);
            byteBuffer.put(STX);
            for (byte input : data) {
                if (input == STX || input == (byte) 5 || input == (byte) 4) {
                    byteBuffer.put((byte) 5);
                    byteBuffer.put(input);
                } else {
                    byteBuffer.put(input);
                }
            }
            byte[] crcBytes = crc(data);
            if (crcBytes[0] == STX || crcBytes[0] == (byte) 5 || crcBytes[0] == (byte) 4) {
                byteBuffer.put((byte) 5);
            }
            byteBuffer.put(crcBytes[0]);
            if (crcBytes[1] == STX || crcBytes[1] == (byte) 5 || crcBytes[1] == (byte) 4) {
                byteBuffer.put((byte) 5);
            }
            byteBuffer.put(crcBytes[1]);
            byteBuffer.put((byte) 4);
            byteBuffer.flip();
            this.mSerialPort.write(byteBuffer, byteBuffer.limit());
        }
    }

    public byte[] read() throws IOException {
        try {
            byte firstByte = readByte();
            byte secondByte = readByte();
            Log.d(TAG, "First byte :" + firstByte + ", Second byte :" + secondByte);
            if (firstByte == STX && secondByte == STX) {
                ByteBuffer output = ByteBuffer.allocate(261);
                boolean escaped = false;
                while (true) {
                    byte b = readByte();
                    if (b == (byte) 4 && !escaped) {
                        break;
                    } else if (b != (byte) 5 || escaped) {
                        escaped = false;
                        output.put(b);
                    } else {
                        escaped = true;
                    }
                }
                output.flip();
                short command = (short) (output.get() & 255);
                Log.d(TAG, "Command " + command);
                if (command <= (short) 0 || (command & 64) == 0) {
                    Log.d(TAG, "Command NACK");
                    short length = (short) output.get();
                    short errorCode = (short) (output.get() & 255);
                    Log.d(TAG, "Error code " + errorCode);
                    throw new BootLoaderMessageError(errorCode);
                }
                Log.d(TAG, "Command ACK");
                short dataLength = (short) (output.get() & 255);
                Log.d(TAG, "Data length " + dataLength);
                if (dataLength == (short) 0) {
                    return null;
                }
                byte[] outputData = new byte[(dataLength + 2)];
                outputData[0] = (byte) (command & 255);
                outputData[1] = (byte) (dataLength & 255);
                output.get(outputData, 2, dataLength);
                byte crcH = output.get();
                byte crcL = output.get();
                byte[] dataCrc = crc(outputData);
                if (dataCrc[0] == crcH || dataCrc[1] == crcL) {
                    return outputData;
                }
                throw new BootLoaderMessageError((short) -2);
            }
            throw new BootLoaderMessageError((short) -1);
        } catch (TimeoutException e) {
            Log.d(TAG, "IO Timed out");
            throw new BootLoaderMessageError((short) -3);
        }
    }

    private int parseInt(byte[] data, int startOffset) {
        return ((0 | ((short) (data[startOffset] & 255))) << 8) | ((short) (data[startOffset + 1] & 255));
    }

    private byte readByte() throws IOException, TimeoutException {
        int tries = 0;
        boolean interrupted = false;
        do {
            ByteBuffer buffer = ByteBuffer.allocate(1);
            int result = this.mSerialPort.read(buffer);
            if (result < 0) {
                throw new IOException("EOF");
            } else if (result == 0) {
                Log.d(TAG, "Read timed out, retrying");
                tries++;
                if (tries >= 10) {
                    break;
                }
                interrupted = Thread.interrupted();
            } else {
                return (byte) (buffer.get() & 255);
            }
        } while (!interrupted);
        if (interrupted) {
            throw new InterruptedIOException();
        }
        throw new TimeoutException();
    }

    private byte[] crc(byte[] data) {
        int crc = Utility.calculateCcittCrc(data);
        return new byte[]{(byte) (crc & 255), (byte) ((crc >> 8) & 255)};
    }

    private void clearBuffers() {
        this.mChunkBuffer = null;
        this.mPacketBuffer = null;
    }
}
