package com.navdy.obd.update;

public interface ObdDeviceFirmwareManager {
    String getFirmwareVersion();

    boolean isUpdatingFirmware();

    boolean updateFirmware(Update update);
}
