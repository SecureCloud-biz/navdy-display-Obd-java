@echo off
pushd %~dp0

if "%1" == "" (
    echo Usage: %0 filename.jar
    exit /b
)

set "TOOLS=%CD%\..\..\.."
set "smali=%CD%\..\smali"

set "PATH=%PATH%;%TOOLS%\dex-tools-2.1-SNAPSHOT;%TOOLS%\dex-tools-2.1-SNAPSHOT\bin;C:\Program Files (x86)\Android\android-sdk\build-tools\25.0.3;C:\Users\corona\AppData\Local\Android\sdk\build-tools\26.0.2;F:\Android\sdk\build-tools\25.0.1"

set "in=%1"

echo Extracting file %in%...

call d2j-jar2dex --force --output %in%.dex %in% 

java -Xmx1500m -jar %TOOLS%\baksmali-2.2.2.jar d --use-locals --output %smali% %in%.dex

del %in%.dex

popd
